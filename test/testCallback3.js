const callBack3 = require('../callback3.js');


// Verifying that it does not work when the file does not exist
callBack3("../boardsasdasd.json", "qwsa221", (err, data) => {
    console.log(err);
});

// Verifying that it does not work
callBack3("../cards.json", "qwsa221111", (err, data) => {
    console.log(data == null);
});

// Verifying that it does not work when the file has bad json data
callBack3("../boards-corrupt.json", "qwsa221", (err, data) => {
    console.log(err);
});


// Verifying that it works
callBack3("../cards.json", "qwsa221", (err, data) => {
    console.log(data);
});

