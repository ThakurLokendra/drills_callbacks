const callBack2 = require("../callback2.js");


// Verifying that it does not work when the file does not exist
callBack2("../boardsasdasd.json", "mcu453ed11111", (err, data) => {
    console.log(err);
});

// Verifying that it does not work
callBack2("../lists.json", "mcu453ed11111", (err, data) => {
    console.log(data == null);
});

// Verifying that it does not work when the file has bad json data
callBack2("../boards-corrupt.json", "mcu453ed11111", (err, data) => {
    console.log(err);
});


// Verifying that it works
callBack2("../lists.json", "mcu453ed", (err, data) => {
    console.log(data);
});

