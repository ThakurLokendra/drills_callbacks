const callBack1 = require('../callback1.js');


// Verifying that it does not work when the file does not exist
callBack1("../boardsasdasd.json", "mcu453ed11111", (err, data) => {
    console.log(err);
});

// Verifying that it does not work
callBack1("../boards.json", "mcu453ed11111", (err, data) => {
    console.log(data == null);
});

// Verifying that it does not work when the file has bad json data
callBack1("../boards-corrupt.json", "mcu453ed11111", (err, data) => {
    console.log(err);
});


// Verifying that it works
callBack1("../boards.json", "mcu453ed", (err, data) => {
    console.log(data);
});


