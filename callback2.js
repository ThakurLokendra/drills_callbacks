const fs = require('fs');

function callBack2(jsonFilePath, boardId, callBack) {

    if (jsonFilePath && boardId && callBack) {

        setTimeout(() => {

            fs.readFile(jsonFilePath, (err, data) => {

                if (err) {
                    callBack(new Error("file is not present"));
                } else {

                    try {
                        let listData = JSON.parse(data);
                        let idPresentOrNot = false;

                        for (const key in listData) {

                            if (key === boardId) {
                                idPresentOrNot = true;
                                callBack(null, listData[key]);
                            }

                        }

                        if (idPresentOrNot == false) {
                            callBack(new Error("Id is not present"));
                        }

                    } catch (error) {

                        if (error instanceof SyntaxError) {
                            callBack(new Error('file not in json format'));
                        } else {
                            callBack(new Error(error));
                        }

                    }

                }

            })

        }, 2 * 1000);

    } else {

        if (!callBack && !jsonFilePath && !boardId) {
            console.log("callback function, json file path and id are not defined");
        } else {
            console.log("function did not recieve some arguements");

        }
    }

}


module.exports = callBack2;