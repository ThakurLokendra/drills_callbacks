const callBack1 = require('./callback1.js');
const callBack2 = require('./callback2.js');
const callBack3 = require('./callback3.js');
const fs = require('fs');

function callBack6() {

    setTimeout(() => {

        callBack1("../boards.json", "mcu453ed", (error, boardData) => {

            if (error) {
                console.log(error);
            } else {
                console.log(boardData);
            }

        });


        callBack2("../lists.json", "mcu453ed", (error, listData) => {

            if (error) {
                console.log(error);
            } else {
                console.log(listData);
            }

        });

        fs.readFile("../cards.json", (error, data) => {
            if (error) {
                console.log("file not found");
            } else {
                try {
                    let cards = JSON.parse(data);
                    for (const key in cards) {
                        callBack3('../cards.json', key, (error, cardsData) => {

                            if (error) {
                                console.log(error);
                            } else {
                                console.log(cardsData);
                            }

                        });
                    }
                } catch (error) {

                    if (error instanceof SyntaxError) {
                        console.log("file not in json formate")
                    } else {
                        console.log(error);
                    }
                }
            }

        })


    }, 2 * 1000);

}



module.exports = callBack6;