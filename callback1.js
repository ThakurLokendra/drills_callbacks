const fs = require('fs');


function callBack1(jsonFilePath, boardId, callBack) {

    if (jsonFilePath && boardId && callBack) {

        setTimeout(() => {

            fs.readFile(jsonFilePath, (error, data) => {

                if (error) {
                    callBack(new Error('file is not present'));
                } else {

                    try {
                        let boardsData = JSON.parse(data);
                        let idPresentOrNot = false;

                        for (let index = 0; index < boardsData.length; index++) {

                            if (boardsData[index]['id'] === boardId) {
                                idPresentOrNot = true;
                                callBack(null, boardsData[index]);
                            }

                        }

                        if (idPresentOrNot == false) {
                            callBack(new Error("Id is not present"));
                        }

                    } catch (error) {

                        if (error instanceof SyntaxError) {
                            callBack(new Error('file not in json format'));
                        } else {
                            callBack(new Error(error));
                        }

                    }

                }

            })

        }, 2 * 1000);

    } else {

        if (!callBack && !jsonFilePath && !boardId) {
            console.log("callback function, json file path and id are not defined");
        } else {
            console.log("function did not recieve some arguements");

        }

    }

}


module.exports = callBack1;