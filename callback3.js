const fs = require('fs');

function callBack3(jsonFilePath, listId, callBack) {

    if (jsonFilePath && listId && callBack) {

        setTimeout(() => {

            fs.readFile(jsonFilePath, (err, data) => {

                if (err) {
                    callBack(new Error('File is not present'));
                } else {

                    try {
                        let cardsData = JSON.parse(data);
                        let idPresentOrNot = false;
                        for (const key in cardsData) {
                            if (key === listId) {
                                idPresentOrNot = true;
                                callBack(null, cardsData[key]);
                            }
                        }

                        if (idPresentOrNot == false) {
                            callBack(new Error('Id is not present'));
                        }

                    } catch (error) {

                        if (error instanceof SyntaxError) {
                            callBack(new Error('file not in json format'));
                        } else {
                            callBack(new Error(error));
                        }

                    }


                }

            })

        }, 2 * 1000);

    } else {

        if (!callBack && !jsonFilePath && !boardId) {
            console.log("callback function, json file path and id are not defined");
        } else {
            console.log("function did not recieve some arguements");

        }
    }

}


module.exports = callBack3;