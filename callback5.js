const callBack1 = require('./callback1.js');
const callBack2 = require('./callback2.js');
const callBack3 = require('./callback3.js');



function callBack5() {

    setTimeout(() => {

        callBack1("../boards.json", "mcu453ed", (error, boardData) => {

            if (error) {
                console.log(error);
            } else {
                console.log(boardData);
            }

        });

        callBack2("../lists.json", "mcu453ed", (error, listData) => {

            if (error) {
                console.log(error);
            } else {
                console.log(listData);
            }

        });


        callBack3("../cards.json", "qwsa221", (error, mindCardData) => {

            if (error) {
                console.log(error);
            } else {

                callBack3("../cards.json", "jwkh245", (error, spaceCardData) => {

                    if (error) {
                        console.log(error);
                    } else {
                        console.log(mindCardData);
                        console.log(spaceCardData);
                    }

                });

            }

        });

    }, 2 * 1000);

}



module.exports = callBack5;