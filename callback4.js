const callBack1 = require('./callback1.js');
const callBack2 = require('./callback2.js');
const callBack3 = require('./callback3.js');



function callBack4() {

    setTimeout(() => {

        callBack1("../boards.json", "mcu453ed", (error, boardData) => {

            if (error) {
                console.log(error);
            } else {
                console.log(boardData);
            }

        });


        callBack2("../lists.json", "mcu453ed", (error, listData) => {

            if (error) {
                console.log(error);
            } else {
                console.log(listData);
            }

        });

        callBack3("../cards.json", "qwsa221", (error, cardsData) => {

            if (error) {
                console.log(error);
            } else {
                console.log(cardsData);
            }

        });

    }, 2 * 1000);

}



module.exports = callBack4;